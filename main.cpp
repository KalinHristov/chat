#include "communicationmanager.h"

#include <QApplication>
#include <QtCore>
#include <QtGui>
#include <QDebug>

int main(int argc, char *argv[])
{
    QApplication app(argc, argv);

    CommunicationManager client;
    client.ui()->show();

    const int result = app.exec();

    return result;
}
