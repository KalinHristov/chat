#include "message.h"

Message::Message(bool aSender, int aPort, QString aText)
{
    m_sender = aSender;
    m_port = aPort;
    m_text = aText;
}

QVariant Message::GetData(int column) const
{
    switch(column)
    {
        case 0:
            return m_sender;
        case 1:
            return m_port;
        case 2:
            return m_text;
    }
}
