#include "mainwindow.h"

#include <QFontInfo>
#include <QShortcut>
#include <QSize>
#include <QtNetwork>
#include <QtWidgets>

MainWindow::MainWindow(QWidget *parent)
    : QWidget(parent)
    , m_ipEditor(new QLineEdit)
    , m_portEditor(new QLineEdit)
    , m_connectButton(new QPushButton("Connect"))
    , m_messagesTable(new QTableView)
    , m_messageEditor(new QTextEdit)
    , m_sendMessageButton(new QPushButton("Send"))
{
    QLabel *ipLabel = new QLabel(tr("IP:"));
    ipLabel->setBuddy(m_ipEditor);
    QLabel *portLabel = new QLabel(tr("Port:"));
    portLabel->setBuddy(m_portEditor);

    m_portEditor->setValidator(new QIntValidator(std::numeric_limits< quint16 >::min(), std::numeric_limits< quint16 >::max(), this));

    const int portDigitsMaxCount = static_cast< int >( floor( log10( std::numeric_limits< quint16 >::max() ) + 1 ) );
    const QString samplePortNumberRaw( portDigitsMaxCount + 2, QChar( QLatin1Char( '9' ) ) );
    const int portEditorWidth = this->fontMetrics().width( samplePortNumberRaw );

    m_portEditor->setFixedWidth( portEditorWidth );

    QHBoxLayout* connectionLayout = new QHBoxLayout;
    connectionLayout->addWidget(ipLabel);
    connectionLayout->addWidget(m_ipEditor);
    connectionLayout->addWidget(portLabel);
    connectionLayout->addWidget(m_portEditor);
    connectionLayout->addWidget(m_connectButton);

    QSplitter* splitter = new QSplitter(Qt::Vertical);
    splitter->addWidget(m_messagesTable);
    splitter->addWidget(m_messageEditor);

    QVBoxLayout* const mainLayout = new QVBoxLayout(this);
    mainLayout->addLayout(connectionLayout);
    mainLayout->addWidget(splitter);
    mainLayout->addWidget(m_sendMessageButton);

    connect(m_connectButton, SIGNAL(clicked()), this, SLOT(connectButtonClicked()));
    connect(m_sendMessageButton, SIGNAL(clicked()), this, SLOT(sendMessage()));
}

void MainWindow::connectButtonClicked()
{
    m_sendMessageButton->setDefault(true);
    connectButtonSignal();
}

void MainWindow::sendMessage()
{
    sendMessageSignal();
    messageEditor()->clear();
}

void MainWindow::scrollTable(){
    m_messagesTable->setSizeAdjustPolicy( QAbstractScrollArea::AdjustToContents );
    m_messagesTable->resizeColumnsToContents();
    m_messagesTable->scrollToBottom();

    qDebug() << "scroll table";
}

void MainWindow::setModel(QAbstractItemModel* model) {
    m_messagesTable->setModel(model);
    m_messagesTable->setSelectionMode(QAbstractItemView::NoSelection);
    m_messagesTable->verticalHeader()->hide();
}

QLineEdit* MainWindow::ipEditor(){
    return m_ipEditor;
}

QLineEdit* MainWindow::portEditor(){
    return m_portEditor;
}

QTextEdit* MainWindow::messageEditor(){
    return m_messageEditor;
}

QPushButton* MainWindow::connectButton() {
    return m_connectButton;
}
