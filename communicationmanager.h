#ifndef COMMANDMANAGER_H
#define COMMANDMANAGER_H

#include "messagesmodel.h"
#include "mainwindow.h"

#include "QTcpServer"

QT_FORWARD_DECLARE_CLASS(QTcpSocket)

class CommunicationManager : public QWidget
{
    Q_OBJECT

public:
    CommunicationManager(QWidget *parent = Q_NULLPTR);
    MainWindow* ui();

private slots:
    void connectionAccepted();
    void readSocket();
    void connectButtonClicked();
    void sendMessageClicked();
    void displayError(QAbstractSocket::SocketError socketError);
    void scrollTable();
    void socketIsConnected();
    void socketIsDisconnected();

private:
    QTcpServer* m_server;
    QTcpSocket* m_tcpSocket;
    MessagesModel* m_messagesModel;

    MainWindow* m_UI;
};

#endif // COMMANDMANAGER_H
