#ifndef CLIENT_H
#define CLIENT_H

#include <QDialog>
#include <QTcpSocket>
#include <QDataStream>
#include <QTextEdit>
#include <QTableView>
#include <QPushButton>
#include <QLineEdit>

#include "messagesmodel.h"

QT_FORWARD_DECLARE_CLASS(QLineEdit)
QT_FORWARD_DECLARE_CLASS(QPushButton)

class MainWindow : public QWidget
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = Q_NULLPTR);

    QLineEdit* ipEditor();
    QLineEdit* portEditor();
    QTextEdit* messageEditor();

    QPushButton* connectButton();

    void scrollTable();
    void setModel(QAbstractItemModel* model);

signals:
    void connectButtonSignal();
    void sendMessageSignal();

private slots:
    void connectButtonClicked();
    void sendMessage();

private:
    QLineEdit* m_ipEditor;
    QLineEdit* m_portEditor;
    QPushButton* m_connectButton;

    QTableView* m_messagesTable;
    QTextEdit* m_messageEditor;
    QPushButton* m_sendMessageButton;
};

#endif // CLIENT_H
